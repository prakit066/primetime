//
//  HistoryTableViewController.swift
//  PrimeTime
//
//  Created by Prakit Kongkaew on 30/7/2562 BE.
//  Copyright © 2562 Prakit. All rights reserved.
//

import UIKit

class HistoryTableViewController: UITableViewController {
    
    var historyData :[String] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyData.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = historyData[indexPath.row]
        return cell
    }
}
