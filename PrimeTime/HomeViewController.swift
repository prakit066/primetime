//
//  ViewController.swift
//  PrimeTime
//
//  Created by Prakit Kongkaew on 30/7/2562 BE.
//  Copyright © 2562 Prakit. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var numberTextField: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var resultButton: UIButton!
    var history : [String] = []
    var image : UIImage!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        image = UIImage(named: "Oval")
        let tintedImage = image?.withRenderingMode(.alwaysTemplate)
        resultButton.setBackgroundImage(tintedImage, for: .normal)
        title =   "Prime Time"
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "GotoHistory",
            let destinationVC = segue.destination as? HistoryTableViewController {
            destinationVC.historyData = history
        }
    }
    
    @IBAction func submited(_ sender: UIButton) {
        guard let stringNumber = numberTextField.text else {
            return
        }
        guard let number = Int(stringNumber) else {
            return
        }
        var isPrime = !(number % 2 == 0 && number > 2)
        if(number == 1 ){
            isPrime = false
        }
        let n : Int =  Int(sqrt( Float(number))) + 1
        if(n >= 3) {
            for x in 3...n {
                if(number%x == 0 ){
                    isPrime = false
                    break
                }
            }
        }
       
        
        if(isPrime) {
            resultButton.tintColor = UIColor.red
            resultButton.setTitle("prime", for: .normal)
            history.append(String( number) + " is prime")
        }else {
            resultButton.tintColor = UIColor.blue
            resultButton.setTitle("not prime", for: .normal)
            history.append(String( number) + " is not prime")
        }
        while(history.count > 10){
            history.remove(at: 0)
        }
        
    }
    


}

